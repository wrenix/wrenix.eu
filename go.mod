module wrenix.eu

go 1.23.2

require (
	codeberg.org/wrenix/flux-charts v0.0.0-20241111111801-09752b52215b // indirect
	codeberg.org/wrenix/helm-charts v0.0.0-20241108144325-1d57a623fc3e // indirect
	github.com/imfing/hextra v0.8.6 // indirect
)
