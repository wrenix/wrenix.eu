#!/bin/sh

AVATAR="avatar.png"

cd static/images

convert $AVATAR -background white -flatten avatar.jpg
convert $AVATAR favicon.png
convert $AVATAR logo@2x.png
convert $AVATAR -resize 50% logo.png

convert $AVATAR -resize x57 apple-touch-icon-57x57.png
convert $AVATAR -resize x72 apple-touch-icon-72x72.png
convert $AVATAR -resize x120 apple-touch-icon-120x120.png
convert $AVATAR -resize x144 apple-touch-icon-144x144.png

# used https://image2svg.com/
# autotrace -output-format svg avatar.jpg  > bimi.svg