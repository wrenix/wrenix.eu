---
title: "About Me"
type: about
---

# WrenIX

I call myself WrenIX.

I use the small slogan:
> The wren in the network.

![blue bird](/images/avatar.png)

## Contakt

E-Mail: [contact@wrenix.eu](mailto:contact@wrenix.eu)

Matrix: [@wrenix:chaos.fyi](https://matrix.to/#/@wrenix:chaos.fyi)

## Cryptography

SSH: `ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGONGPQ79A9WZ7EwM6vMfBKBkgPD2dsjExFoo2UXyd79 dev@wrenix.eu`
 * [Download](/keys/ssh.txt)

GPG-Fingerprint: `B9C3 5FDD 7362 F063 A870  6A2E 7AFD B012 974B 1BB5`
 * [Armour](/keys/dev.gpg.txt)
 * [Binary](/keys/dev.gpg.bin)
