---
title: WrenIX
toc: false
---

This is my privat homepage.

## Explore

{{< cards >}}
  {{< card link="blog" title="Blog" icon="newspaper" >}}
  {{< card link="docs" title="Docs" icon="book-open" >}}
  {{< card link="about" title="About Me" icon="user" >}}
{{< /cards >}}
