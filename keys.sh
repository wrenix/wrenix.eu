#!/bin/sh

KEY_ID=B9C35FDD7362F063A8706A2E7AFDB012974B1BB5

gpg2 --no-armour -o static/keys/dev.gpg.bin --export $KEY_ID
gpg2 --armour -o static/keys/dev.gpg.txt --export $KEY_ID
